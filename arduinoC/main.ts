//% color="#D2691E" iconWidth=40 iconHeight=40
namespace fingerprint {
  //% board="arduino "
  //% block="fingerprint sensor initliallize pin [SSER] RX [SSRXD] TX [SSTXD]   blockType="command"
  //% SSER.shadow="dropdown" SSER.options="SSER"
  //% SSTXD.shadow="dropdown" SSTXD.options="SSTXD"
  //% SSRXD.shadow="dropdown" SSRXD.options="SSRXD"
  //% BO.shadow="dropdown" BO.options="BO"
  export function beginSoftSerial(parameter: any, block: any) {
    let tx = parameter.SSTXD.code;
    let rx = parameter.SSRXD.code;
    let sser = "finger" + parameter.SSER.code;
  

      Generator.addInclude("includeFingerprint","#include <Adafruit_Fingerprint.h>");
      Generator.addInclude("includesoftSerial", "#include <SoftwareSerial.h>");
      Generator.addObject("softSerialObject","SoftwareSerial",`${sser}(${rx}, ${tx});`);
      Generator.addObject("FingerObject","Adafruit_Fingerprint",`finger = Adafruit_Fingerprint();`);
    Generator.addSetup("FingerSoftSerialSetup", `finger.begin(&${sser});`);
  }
  //% board=" esp32"
  //% block="fingerprint sensor initliallize pin [SER] RX [RXD] TX [TXD] BAUDRATE[BAUDRATE] " blockType="command"
  //% SER.shadow="dropdown" SER.options="SER"
  //% RXD.shadow="dropdown" RXD.options="RXD"
  //% TXD.shadow="dropdown" TXD.options="TXD"
  //% BAUDRATE.shadow="dropdown" BAUDRATE.options="BAUDRATE"
  export function beginSerial(parameter: any, block: any) {
    let ser = parameter.SER.code;
    let rx = parameter.RXD.code;
    let tx = parameter.TXD.code;
    let bo = parameter.BAUDRATE.code;
      Generator.addInclude("includeFingerprint", "#include <Adafruit_Fingerprint.h>");
      Generator.addObject("FingerObject", "Adafruit_Fingerprint", `finger = Adafruit_Fingerprint( );` );
   
    if (Generator.board === "microbit") {
      Generator.addSetup("FingerSerialSetup",`finger.begin(&${ser}, ${rx}, ${tx},${bo});`);
    } else if (Generator.board === "esp32") {
      Generator.addSetup("FingerSerialSetup",`finger.begin(&${ser}, ${rx}, ${tx},${bo});`);
    }
  }
  //% board="arduino,esp32 "
  //% block="fingerID [ID]" blockType="command"
  //% ID.shadow="number" ID.defl="1"
  export function fingerIDlu(parameter: any, block: any) {
    let id = parameter.ID.code;
      Generator.addSetup("FingerSerialSetup1",`Serial.begin(9600);`);
      Generator.addCode(`while (!finger.getFingerprintEnroll(${id}));`);

  }
  //% board="arduino,esp32 "
  //% block="deleteModelfingerID[ID]" blockType="command"
  //% ID.shadow="number" ID.defl="1"
  export function fingerIDdeleteModel(parameter: any, block: any) {
    let id = parameter.ID.code;
    if (Generator.board === "arduino") {
      Generator.addCode(`finger.deleteModel(${id});`);
      Generator.addCode(`	Serial.println("删除指纹ID(delete ID)：${id}");`);
    } else if (Generator.board === "esp32") {
      Generator.addInclude("includeFingerprint2", "#include <MPython.h>");
       
      Generator.addSetup("Fingerprint2",`mPython.begin();`);
      Generator.addCode(`finger.deleteModel(${id});`);
     
       
      Generator.addCode(`display.setCursor(22, 22);`);
      Generator.addCode(`display.print("删除指纹ID(delete ID)：${id}");`);
      Generator.addCode(`delay(1000);`);
      Generator.addCode(`display.fillScreen(0);`); 
         
      }
      
    }
    
  //% block="finger getFinger success?" blockType="boolean"
  export function getFingerSuccess() {
      Generator.addCode(["(finger.getImage() == FINGERPRINT_OK) && (finger.image2Tz() == FINGERPRINT_OK) && (finger.fingerFastSearch() == FINGERPRINT_OK)",Generator.ORDER_UNARY_POSTFIX]);
  }

  //% block="Current fingerID" blockType="reporter"
  export function fingerID() {
      Generator.addCode(["finger.fingerID",Generator.ORDER_UNARY_POSTFIX]);
  }
  //% block="fingerID total" blockType="reporter"
  export function fingerchaID() {
      Generator.addCode(["finger.getTemplateCount()",Generator.ORDER_UNARY_POSTFIX] );   
      
    }
} 

